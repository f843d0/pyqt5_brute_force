from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, QMutex, QObject, QThread, pyqtSignal, QTimer
from PyQt5.QtGui import QPixmap, QScreen
import sys
import json
from pathlib import Path
import os
from pynput.keyboard import Key, Controller
from copy import deepcopy
import hashlib

class Worker(QObject):
	finished = pyqtSignal()

	def run(self, main_window):
		current_execution_sequence = main_window.last_execution_sequence
		if not len(current_execution_sequence):
			current_execution_sequence += main_window.brute_force_set[0]
		copy_config_j_data = deepcopy(main_window.config_j_data)
		while True:
			QApplication.processEvents()
			if main_window.please_stop:
				main_window.please_stop = False
				main_window.is_thread_running = False
				main_window.set_gui_disabled(False)
				self.finished.emit()
				return
			# Here is the robot part
			for char in current_execution_sequence:
				main_window.keyboard.press(char)
				main_window.keyboard.release(char)
			for it, val in enumerate(copy_config_j_data['robot_directives']):
				l = list(*val.items())
				if l[0] == "shift":
					main_window.keyboard.press(Key.shift)
				elif l[0] == "unshift":
					main_window.keyboard.release(Key.shift)
				elif l[0] == "tab":
					main_window.keyboard.press(Key.tab)
					main_window.keyboard.release(Key.tab)
				elif l[0] == "enter":
					main_window.keyboard.press(Key.enter)
					main_window.keyboard.release(Key.enter)
				else:
					main_window.keyboard.press(l[0])
					main_window.keyboard.release(l[0])
				l[1] = str(int(l[1]) - 1)
				copy_config_j_data['robot_directives'][it][l[0]] = l[1]
				if int(l[1]) < 0:
					copy_config_j_data['robot_directives'][it][l[0]] = main_window.config_j_data['robot_directives'][it][l[0]]
			main_window.save_screenshot('current.bmp', False)


			main_window.last_execution_sequence = current_execution_sequence
			main_window.evaluate_progress()

			if main_window.reference_md5sum != main_window.current_md5sum:
				break

			representation = []
			for char in current_execution_sequence:
				representation.append(main_window.brute_force_set.index(char))
			representation.reverse()
			is_ongoing = False
			for it, val in enumerate(representation):
				if val == len(main_window.brute_force_set) - 1:
					representation[it] = 0
					continue
				is_ongoing = True
				representation[it] = val + 1
				break
			representation.reverse()
			current_execution_sequence = ""
			for value in representation:
				current_execution_sequence += main_window.brute_force_set[value]
			if not is_ongoing:
				current_execution_sequence += main_window.brute_force_set[0]

		main_window.please_stop = False
		main_window.is_thread_running = False
		main_window.set_gui_disabled(False)
		main_window.cli_area.insertPlainText('\n\nPASSWORD FOUND, IT IS:' + main_window.last_execution_sequence)
		self.finished.emit()

class MainWindow(QWidget):
	def __init__(self, application):
		QWidget.__init__(self, None)
		self.root_app_instance = application
		self.resize(320, 550)
		self.setWindowTitle('Cracker')
		self.setWindowFlags(Qt.WindowStaysOnTopHint)

		self.cli_area = QTextEdit(self)
		self.cli_area.setReadOnly(True)
		self.cli_area.setLineWrapMode(QTextEdit.NoWrap)
		self.cli_area.font().setFamily("Courier")

		self.path = os.path.dirname(Path(__file__).resolve())
		with open(os.path.join(self.path, 'conf', 'conf.json')) as json_file:
			self.config_j_data = json.load(json_file)
		self.cli_area.insertPlainText('default_init_timeout=' + self.config_j_data['default_init_timeout'] + '\n')
		self.brute_force_set = self.config_j_data['brute_force_set']
		self.cli_area.insertPlainText('brute_force_set=' + str(self.brute_force_set) + '\n')
		self.last_execution_sequence = self.config_j_data['last_execution_sequence']
		self.cli_area.insertPlainText('last_execution_sequence=' + self.last_execution_sequence + '\n')
		self.cli_area.insertPlainText('robot_directives=' + str(self.config_j_data['robot_directives']) + '\n')
		self.cli_area.verticalScrollBar().setValue(self.cli_area.verticalScrollBar().maximum())

		self.label_timeout = QLabel('Wait these seconds before starting the Robot Sequence')
		self.spin_timeout = QSpinBox()
		self.spin_timeout.setMinimum(5)
		self.spin_timeout.setMaximum(3600)
		self.spin_timeout.setValue(int(self.config_j_data['default_init_timeout']))

		self.label_progress = QLabel()
		self.progress_bar = QProgressBar(self)
		self.progress_bar.setMaximum(100)
		self.evaluate_progress()

		self.fire_button = QPushButton('Start!')
		self.fire_button.setDefault(True)
		self.fire_button.setFixedSize(60, 20)
		self.fire_button.clicked.connect(self.start_timer)
		self.timer = QTimer()
		self.timer.timeout.connect(self.perform_brute_force)

		self.screen_button = QPushButton('Take Screenshot')
		self.screen_button.setFixedSize(150, 20)
		self.screen_button.clicked.connect(lambda: self.save_screenshot('reference.bmp', True))
		self.reference_md5sum = None
		self.label_x_0 = QLabel('x0:')
		self.spin_x_0 = QSpinBox()
		self.spin_x_0.setMinimum(0)
		self.spin_x_0.setMaximum(10000)
		self.spin_x_0.setValue(110)
		self.label_x_1 = QLabel('x0 offset:')
		self.spin_x_1 = QSpinBox()
		self.spin_x_1.setMinimum(0)
		self.spin_x_1.setMaximum(10000)
		self.spin_x_1.setValue(10)
		self.label_y_0 = QLabel('y0:')
		self.spin_y_0 = QSpinBox()
		self.spin_y_0.setMinimum(0)
		self.spin_y_0.setMaximum(10000)
		self.spin_y_0.setValue(140)
		self.label_y_1 = QLabel('y0 offset:')
		self.spin_y_1 = QSpinBox()
		self.spin_y_1.setMinimum(0)
		self.spin_y_1.setMaximum(10000)
		self.spin_y_1.setValue(10)
		self.screen_preview = QLabel()

		self.is_thread_running = False
		self.please_stop = False

		self.root_layout = QVBoxLayout(self)

		self.timeout_group_box = QGroupBox('Timeout', self)
		self.timeout_layout = QHBoxLayout()
		self.timeout_layout.addWidget(self.label_timeout)
		self.timeout_layout.addWidget(self.spin_timeout)
		self.timeout_group_box.setLayout(self.timeout_layout)

		self.progress_group_box = QGroupBox('Progress', self)
		self.progress_layout = QVBoxLayout()
		self.progress_layout.addWidget(self.label_progress)
		self.progress_layout.addWidget(self.progress_bar)
		self.progress_group_box.setLayout(self.progress_layout)

		self.cli_area_group_box = QGroupBox('Status', self)
		self.cli_area_layout = QVBoxLayout()
		self.cli_area_layout.addWidget(self.cli_area)
		self.cli_area_group_box.setLayout(self.cli_area_layout)

		self.screenshot_group_box = QGroupBox('Screenshot', self)
		self.screenshot_layout_main = QVBoxLayout()
		self.screenshot_layout_inner_1 = QHBoxLayout()
		self.screenshot_layout_column_0 = QVBoxLayout()
		self.screenshot_layout_row_0_0 = QHBoxLayout()
		self.screenshot_layout_row_0_1 = QHBoxLayout()
		self.screenshot_layout_column_1 = QVBoxLayout()
		self.screenshot_layout_row_1_0 = QHBoxLayout()
		self.screenshot_layout_row_1_1 = QHBoxLayout()

		self.screenshot_layout_row_0_0.addWidget(self.label_x_0, alignment = Qt.AlignRight)
		self.screenshot_layout_row_0_0.addWidget(self.spin_x_0)
		self.screenshot_layout_row_0_1.addWidget(self.label_y_0, alignment = Qt.AlignRight)
		self.screenshot_layout_row_0_1.addWidget(self.spin_y_0)

		self.screenshot_layout_column_0.addLayout(self.screenshot_layout_row_0_0)
		self.screenshot_layout_column_0.addLayout(self.screenshot_layout_row_0_1)

		self.screenshot_layout_row_1_0.addWidget(self.label_x_1, alignment = Qt.AlignRight)
		self.screenshot_layout_row_1_0.addWidget(self.spin_x_1)
		self.screenshot_layout_row_1_1.addWidget(self.label_y_1, alignment = Qt.AlignRight)
		self.screenshot_layout_row_1_1.addWidget(self.spin_y_1)

		self.screenshot_layout_column_1.addLayout(self.screenshot_layout_row_1_0)
		self.screenshot_layout_column_1.addLayout(self.screenshot_layout_row_1_1)

		self.screenshot_layout_inner_1.addLayout(self.screenshot_layout_column_0)
		self.screenshot_layout_inner_1.addLayout(self.screenshot_layout_column_1)
		self.screenshot_layout_inner_1.addWidget(self.screen_preview, alignment = Qt.AlignCenter)

		self.screenshot_layout_main.addLayout(self.screenshot_layout_inner_1)
		self.screenshot_layout_main.addWidget(self.screen_button, alignment = Qt.AlignCenter)
		self.screenshot_group_box.setLayout(self.screenshot_layout_main)

		self.root_layout.addWidget(self.timeout_group_box)
		self.root_layout.addWidget(self.progress_group_box)
		self.root_layout.addWidget(self.screenshot_group_box)
		self.root_layout.addWidget(self.cli_area_group_box)
		self.root_layout.addWidget(self.fire_button, alignment = Qt.AlignCenter)
		self.setLayout(self.root_layout)

		self.keyboard = Controller()

	def take_screenshot(self):
		self.screen_preview.setPixmap(
				QScreen.grabWindow(
					self.root_app_instance.primaryScreen(),
					QApplication.desktop().winId(),
					self.spin_x_0.value(),
					self.spin_y_0.value(),
					self.spin_x_1.value(),
					self.spin_y_1.value()
				))
		self.screen_preview.show()
	def save_screenshot(self, filename, is_reference):
		if is_reference:
				self.take_screenshot()
		file_path = os.path.join(self.path, filename)
		QScreen.grabWindow(self.root_app_instance.primaryScreen(),
				QApplication.desktop().winId(),
				self.spin_x_0.value(),
				self.spin_y_0.value(),
				self.spin_x_1.value(),
				self.spin_y_1.value()
				).save(file_path, 'bmp')
		if is_reference:
			self.reference_md5sum = hashlib.md5(open(file_path, 'rb').read()).hexdigest()
		else:
			self.current_md5sum = hashlib.md5(open(file_path, 'rb').read()).hexdigest()

	def evaluate_progress(self):
		self.label_progress.setText('Progress on current Brute Force:' + str(len(self.last_execution_sequence)) + ' symbols in sequence ' + str(len(self.brute_force_set)) + ' long')

		entries = 0
		total = 0
		if not len(self.last_execution_sequence):
			self.progress_bar.setValue(0)
			return
		for it, char in enumerate(self.last_execution_sequence):
			position_weight = len(self.last_execution_sequence) - it - 1
			sequence_len = len(self.brute_force_set)
			total += sequence_len ** (position_weight + 1)
			element = self.brute_force_set.index(char) + 1
			entries += element * (sequence_len ** position_weight)
		self.progress_bar.setValue(100 * entries / total)
	def start_timer(self):
		if not self.reference_md5sum:
			QMessageBox.critical(self, 'Critical', 'No Screenshot has been taken, cannot proceed', QMessageBox.Ok)
			return
		if not len(self.brute_force_set):
			QMessageBox.critical(self, 'Critical', 'The Brute Force Set is empty, please fill one in configuration file', QMessageBox.Ok)
			return
		if not self.is_thread_running:
			self.timer.start(self.spin_timeout.value() * 1000)
		else:
			self.timer.start(42)
	def set_gui_disabled(self, is_gui_disabled):
		self.fire_button.setDisabled(is_gui_disabled)
		self.screen_button.setDisabled(is_gui_disabled)
		self.spin_timeout.setDisabled(is_gui_disabled)
		self.spin_x_0.setDisabled(is_gui_disabled)
		self.spin_y_0.setDisabled(is_gui_disabled)
		self.spin_x_1.setDisabled(is_gui_disabled)
		self.spin_y_1.setDisabled(is_gui_disabled)
	def perform_brute_force(self):
		self.timer.stop()
		self.set_gui_disabled(True)
		if self.is_thread_running:
			self.please_stop = True
			return
		self.is_thread_running = True
		self.thread = QThread()
		self.worker = Worker()
		self.worker.moveToThread(self.thread)
		self.thread.started.connect(lambda: self.worker.run(self))
		self.worker.finished.connect(self.thread.quit)
		self.worker.finished.connect(self.worker.deleteLater)
		self.thread.finished.connect(self.thread.deleteLater)
		self.thread.start()
	def closeEvent(self, event):
		if self.is_thread_running:
			self.please_stop = True
			reply = QMessageBox.question(self, 'Message', 'A Brute Forcing was ongoing. Are you really sure you want to quit?', QMessageBox.Yes, QMessageBox.No)
			if reply == QMessageBox.No:
				event.ignore()
				return
		event.accept()

root = QApplication([])
app = MainWindow(root)
app.show()
sys.exit(root.exec_())
