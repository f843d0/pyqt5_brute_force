from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
import sys

class MainWindow(QWidget):
    def on_button_clicked(self):
        if self.textbox.text() == 'foo':
            self.label.setText('Password Correct!')
        else:
            self.label.setText('Invalid password')
    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        self.resize(320, 200)
        self.setWindowTitle('Sample App')
        self.textbox = QLineEdit(self)
        self.textbox.resize(280, 40)
        self.label = QLabel()
        self.button = QPushButton('OK')
        self.button.setDefault(True)
        self.button.clicked.connect(self.on_button_clicked)
        self.textbox.returnPressed.connect(self.on_button_clicked)
        self.layout = QGridLayout(self)
        self.layout.addWidget(self.textbox, 0, 0, alignment = Qt.AlignCenter)
        self.layout.addWidget(self.label, 1, 0, alignment = Qt.AlignCenter)
        self.layout.addWidget(self.button, 2, 0, alignment = Qt.AlignCenter)
        self.setLayout(self.layout)

root = QApplication([])
app = MainWindow()
app.show()
sys.exit(root.exec_())
